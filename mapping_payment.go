package library

import "net/http"

func MappingStatusPaymentOY(statusCode string) (code int, status int, message string) {
	switch statusCode {
	case "101", "000":
		code = ERR_SUCCESS
		status = http.StatusOK
		message = "Success"
	case "201":
		code = ERR_PAYMENT_DECLINED
		status = http.StatusBadRequest
		message = "User ID is not Found"
	case "202":
		code = ERR_PAYMENT_DECLINED
		status = http.StatusBadRequest
		message = "User ID/product is not active"
	case "203":
		code = ERR_PAYMENT_DECLINED
		status = http.StatusBadRequest
		message = "Duplicate Partner Tx Id"
	case "204":
		code = ERR_PAYMENT_DECLINED
		status = http.StatusBadRequest
		message = "Invoice ID is not found"
	case "205":
		code = ERR_PAYMENT_DECLINED
		status = http.StatusBadRequest
		message = "Beneficiary Bank Code is Not Supported"
	case "206":
		code = ERR_NOT_ENOUGH_BALANCE
		status = http.StatusBadRequest
		message = "Balance is not enough"
	case "207":
		code = ERR_PAYMENT_DECLINED
		status = http.StatusBadRequest
		message = "Request IP Address is not Registered"
	case "208":
		code = ERR_PAYMENT_DECLINED
		status = http.StatusBadRequest
		message = "API Key is not Valid"
	case "209":
		code = ERR_PAYMENT_DECLINED
		status = http.StatusBadRequest
		message = "Bank Account is not found"
	case "216":
		code = ERR_PAYMENT_VA_EMPTY
		status = http.StatusBadRequest
		message = "VA Id is empty"
	case "217":
		code = ERR_PAYMENT_VA_ACTIVE
		status = http.StatusBadRequest
		message = "VA Number is still active for this partner user id"
	case "232":
		code = ERR_PAYMENT_DECLINED
		status = http.StatusBadRequest
		message = "User has unpaid invoices"
	case "300":
		code = ERR_PAYMENT_DUPLICATE
		status = http.StatusBadRequest
		message = "Uinvoice is not on UNPAID status"
	case "429":
		code = ERR_PAYMENT_DECLINED
		status = http.StatusBadRequest
		message = "Too Many Request to specific endpoint"
	case "990":
		code = ERR_PAYMENT_DECLINED
		status = http.StatusBadRequest
		message = "Request Parameter is not Valid"
	default:
		code = ERR_OTHERS
		status = http.StatusInternalServerError
		message = "Internal server error"
	}
	return
}

func MappingCheckStatusVA(statusVA string) (code int, status int, message string) {
	switch statusVA {
	case "WAITING_PAYMENT":
		code = ERR_PAYMENT_WAITING
		status = http.StatusOK
		message = "Success"
	case "PAYMENT_DETECTED":
		code = ERR_PAYMENT_DETECTED
		status = http.StatusOK
		message = "Payment Detected"
	case "EXPIRED", "STATIC_TRX_EXPIRED":
		code = ERR_PAYMENT_EXPIRED
		status = http.StatusBadRequest
		message = "Payment Expired"
	case "COMPLETE":
		code = ERR_SUCCESS
		status = http.StatusOK
		message = "Success"
	default:
		code = ERR_OTHERS
		status = http.StatusInternalServerError
		message = "Internal server error"
	}
	return
}
