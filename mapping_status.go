package library

func MappingStatusJne(originCode string) (code int, remark string) {

	switch originCode {
	//PICK
	case "F01", "F02", "F03", "F04", "PR1", "PR2", "REQ", "MP":
		code = ERR_CREATE_PICK
		remark = "PICK"
	//PICKED
	case "PU0", "PU1", "PU2", "S01":
		code = ERR_CREATE_PICKED
		remark = "PICKED"
	//ON PROCESS
	case "BI1", "BI2", "BI3", "RC1", "RC2", "R14", "OP1", "OP2", "OP3", "OP4", "TP1", "TP2", "TP3", "TP4", "TP5", "T13", "T14", "IP1", "IP2", "IP3", "OPX", "OMX", "DP1", "WH5", "102", "305", "307", "403", "914", "915":
		code = ERR_ON_PROCESS
		remark = "ON PROCESS"
	//UNDELIVERY
	case "CL1", "CL2", "CL4", "CR3", "CR5", "CR6", "DP2", "DP3", "DP4", "DP5", "DPU", "KRK", "KRP", "KRT", "OC", "OS", "PS2", "PS3", "PS4", "T01", "T02", "T03", "T04", "T05", "T08", "T09", "T10", "T12", "U01", "U02", "U03", "U04", "U05", "U06", "U07", "U08", "U09", "U12", "U13", "U14", "U21", "U22", "U23", "U24", "U25", "UB1", "UB2", "WH1", "WH3", "WH4", "X1", "X10", "X2", "X31", "X32", "X4", "X5", "X6", "X71", "X72", "X8", "X9", "D26":
		code = ERR_UNDELIVERY
		remark = "UNDELIVERY"
	//RETURN
	case "CR1", "D15", "D16", "CR7", "R26", "CR8", "T11", "R13", "WH2":
		code = ERR_RETURN
		remark = "RETURN"
	//RETURNED
	case "R01", "R02", "R03", "R04", "R05", "R06", "R07", "R08", "R09", "R10", "R11", "R12":
		code = ERR_RETURNED
		remark = "RETURNED"
	//LOST
	case "T06":
		code = ERR_LOST
		remark = "LOST"
	//BROKEN
	case "U11", "PS1", "D24", "R24", "T07":
		code = ERR_BROKEN
		remark = "BROKEN"
	//BREACH
	case "D25", "R25":
		code = ERR_BREACH
		remark = "BREACH"
	//CANCEL PICKUP
	case "CR2", "F05":
		code = ERR_CANCEL_PICKUP
		remark = "CANCEL PICKUP"
	//FORCE MAJEUR
	case "U10", "FOM", "F06":
		code = ERR_FORCE_MAJEUR
		remark = "FORCE MAJEUR"
	//SUCCESS
	case "D01", "D02", "D03", "D04", "D05", "D06", "D07", "D08", "D09", "D10", "D11", "D12", "DB1", "DB2":
		code = ERR_SUCCESS
		remark = "DELIVERED"
	//DESTROYED
	case "CR4": // SESUAI
		code = ERR_DESTROYED
		remark = "DESTROYED"
	//HOLD
	case "HL1", "HL2", "HL3", "HL4":
		code = ERR_HOLD
		remark = "HOLD"
	default:
		code = ERR_ON_PROCESS
		remark = "ON PROCESS"
	}

	return
}

func MappingStatusSiCepat(originCode string) (code int, remark string) {

	switch originCode {
	// SUCCESS CREATE ORDER
	case "PICKREQ":
		code = ERR_SUCCESS_CREATE_ORDER
		remark = "SUCCESS CREATE ORDER"
	//PICK
	case "PICK":
		code = ERR_CREATE_PICK
		remark = "PICK"
	//UNPICK
	case "UNPICK":
		code = ERR_UNPICK
		remark = "UNPICK"
	//DROP
	case "DROP":
		code = ERR_DROP
		remark = "DROP"
	//ON PROCESS
	case "IN", "OUT", "ANT", "OTS", "OSD", "THP", "DETAINED_BY_CUSTOMS", "CANCEL RETURN":
		code = ERR_ON_PROCESS
		remark = "ON PROCESS"
	//SUCCESS
	case "DLV", "DELIVERED":
		code = ERR_SUCCESS
		remark = "DELIVERED"
	//UNDELIVERY
	case "CU", "CNEE UNKNOWN", "NTH", "NOT AT HOME", "AU", "ANTAR ULANG", "BA", "BAD ADDRESS", "CC", "CRISS CROSS", "CODA", "CLOSED ONCE DELIVERY ATTEMPT", "HOLD", "HOLD / PENDING", "CODB", "RBA", "RBC", "CODOC":
		code = ERR_UNDELIVERY
		remark = "UNDELIVERY"
	//LOST
	case "LOST", "HILANG":
		code = ERR_LOST
		remark = "LOST"
	//BROKEN
	case "BROKEN":
		code = ERR_BROKEN
		remark = "BROKEN"
	//DESTROYED
	case "PDA":
		code = ERR_DESTROYED
		remark = "DESTROYED"
	//FORCE MAJEUR
	case "FORCE_MAJEURE", "FORCE MAJEURE":
		code = ERR_FORCE_MAJEUR
		remark = "FORCE MAJEUR"
	//RETURN
	case "RTN", "RETUR PUSAT", "RTA", "UNRTS", "SIW", "RTW", "RTC":
		code = ERR_RETURN
		remark = "RETURN"
	//RETURNED
	case "RTS", "RETURN TO SHIPPER":
		code = ERR_RETURNED
		remark = "RETURNED"
	//CANCEL
	case "CANCEL":
		code = ERR_CANCEL_PICKUP
		remark = "CANCEL ORDER/PICKUP"
	default:
		code = ERR_ON_PROCESS
		remark = "ON PROCESS"
	}

	return
}

func MappingStatusSAP(originCode string) (code int, remark string) {

	switch originCode {
	//PICK
	case "ENTRI (SEDANG DI PICKUP)", "ENTRI (PENDING PICKUP)", "ENTRI (SEDANG PICKUP ULANG)":
		code = ERR_CREATE_PICK
		remark = "PICK"
	//ON PROCESS
	case "ENTRI VERIFIED", "MANIFEST OUTGOING", "INCOMING", "DELIVERY", "OUTGOING SMU", "INCOMING SMU":
		code = ERR_ON_PROCESS
		remark = "ON PROCESS"
	//DELIVERED
	case "POD - DELIVERED":
		code = ERR_SUCCESS
		remark = "DELIVERED"
	//UNDELIVERY
	case "POD - UNDELIVERED":
		code = ERR_UNDELIVERY
		remark = "UNDELIVERY"
	//RETURN
	case "POD - RETURN", "DELIVERY RETURN":
		code = ERR_RETURN
		remark = "RETURN"
	//RETURNED
	case "POD - RETURNED", "SHIPMENT RETURN TO CLIENT":
		code = ERR_RETURNED
		remark = "RETURNED"
	//CANCEL
	case "VOID_PICKUP":
		code = ERR_UNPICK
		remark = "UNPICK"
		//LOST
	case "SHIPMENT LOST":
		code = ERR_LOST
		remark = "LOST"
	default:
		code = ERR_ON_PROCESS
		remark = "ON PROCESS"
	}
	return
}

func MappingStatusNinja(status string) (code int, remark string) {
	switch status {
	case "Pending Pickup", "Parcel Measurements Update":
		code = ERR_CREATE_PICK
		remark = "PICK"
	case "Successful Pickup", "En-route to Sorting Hub", "Picked Up":
		code = ERR_CREATE_PICKED
		remark = "PICKED"
	case "Arrived at Sorting Hub", "Arrived at Origin Hub", "On Vehicle for Delivery", "Arrived at Transit Hub", "In Transit to Next Sorting Hub":
		code = ERR_ON_PROCESS
		remark = "ON PROCESS"
	case "Pickup Fail":
		code = ERR_UNPICK
		remark = "UNPICK"
	case "First Attempt Delivery Fail", "Delivery Exception", "Pending Reschedule":
		code = ERR_UNDELIVERY
		remark = "UNDELIVERED"
	case "On Vehicle for Delivery (RTS)", "Return to Sender Triggered":
		code = ERR_RETURN
		remark = "RETURN"
	case "Returned to Sender":
		code = ERR_RETURNED
		remark = "RETURNED"
	case "Cancelled":
		code = ERR_CANCEL_PICKUP
		remark = "CANCEL"
	case "Completed", "Successful Delivery", "Delivered":
		code = ERR_SUCCESS
		remark = "DELIVERED"
	case "Parcel Lost":
		code = ERR_LOST
		remark = "LOST"
	case "Parcel Damaged":
		code = ERR_BROKEN
		remark = "BROKEN"
	case "Pickup fail":
		code = ERR_UNPICK
		remark = "UNPICK"
	default:
		code = ERR_ON_PROCESS
		remark = "ON PROCESS"
	}
	return
}

func MappingStatusIDExpress(originCode string) (code int, remark string) {

	switch originCode {
	//PICK
	case "Pick up scan":
		code = ERR_CREATE_PICK
		remark = "PICK"
	//ON PROCESS
	case "Arrival scan", "Unloading scan", "Delivery scan", "Loading scan", "Sending scan":
		code = ERR_ON_PROCESS
		remark = "ON PROCESS"
	//DELIVERED
	case "POD scan", "POD Entry":
		code = ERR_SUCCESS
		remark = "DELIVERED"
	//UNDELIVERY
	case "Problem On Shipment scan", "Create Return Bill", "Confirm Return Bill":
		code = ERR_UNDELIVERY
		remark = "UNDELIVERY"
	//RETURN
	case "Return Confirm":
		code = ERR_RETURN
		remark = "RETURN"
	//RETURNED
	case "Return POD Scan":
		code = ERR_RETURNED
		remark = "RETURNED"
	//UNPICK
	case "Pickup Failure":
		code = ERR_UNPICK
		remark = "UNPICK"
	//DROP
	case "Dropped Off at Store":
		code = ERR_DROP
		remark = "DROP OFF AT STORE"
	// CANCEL
	case "Cancel Order New", "Cancel Order":
		code = ERR_CANCEL_PICKUP
		remark = "CANCEL PICKUP"
	default:
		code = ERR_ON_PROCESS
		remark = "ON PROCESS"
	}
	return
}

func MappingStatusProblemIDExpress(problemCode string) (remarkID, remarkEN string) {

	switch problemCode {
	//UNDELIVERY
	case "3001":
		remarkID = "Toko atau kantor sudah tutup"
		remarkEN = "Store or office closed"
	case "3002":
		remarkID = "Pelanggan tidak di lokasi"
		remarkEN = "Recipient is away"
	case "3003":
		remarkID = "Reschedule pengiriman dengan penerima"
		remarkEN = "Re-schedule the delivery time with recipient"
	case "3004":
		remarkID = "Pelanggan membatalkan pengiriman"
		remarkEN = "Shipment cancellation"
	case "3005":
		remarkID = "Pelanggan ingin dikirim ke alamat berbeda"
		remarkEN = "Recipient wants to be sent to another address"
	case "3007":
		remarkID = "Penerima menolak menerima paket"
		remarkEN = "Recipient rejected the package"
	case "3008":
		remarkID = "Alamat pelanggan salah/sudah pindah alamat"
		remarkEN = "Wrong recipient address or Recipient already move to another addres"
	case "3011":
		remarkID = "Paket salah dalam proses penyortiran"
		remarkEN = "Miss Sort"
	case "3012":
		remarkID = "Paket rusak/pecah"
		remarkEN = "Damaged parcels"

	case "3014":
		remarkID = "Data alamat tidak sesuai dengan kode sortir"
		remarkEN = "Destination address does not match with sort code"
	case "3015":
		remarkID = "Penerima ingin membuka paket sebelum membayar"
		remarkEN = "Recipient want to open the parcels before COD payment"
	case "3017":
		remarkID = "Pengemasan paket dengan kemasan rusak"
		remarkEN = "Repackage damage parcels"
	case "3018":
		remarkID = "Paket crosslabel"
		remarkEN = "Crosslabel parcels"
	case "6005":
		remarkID = "Paket makanan, disimpan hingga waktu pengiriman yang tepat"
		remarkEN = "Food parcels, kept until proper delivery time"
	case "6006":
		remarkID = "Pelanggan libur akhir pekan/libur panjang"
		remarkEN = "Recipient take weekend or long holiday"
	case "6007":
		remarkID = "Pelanggan berinisiatif mengambil paket di cabang"
		remarkEN = "Recipient want to self pick-up the parcels to the branch"
	case "6008":
		remarkID = "Melewati jam operasional cabang"
		remarkEN = "Exceeded branch business hours"
	case "6009":
		remarkID = "Cuaca buruk / bencana alam"
		remarkEN = "Bad weather or force majeure"
	case "6010":
		remarkID = "Kerusakan pada resi / informasi resi tidak jelas"
		remarkEN = "Information on AWB is unclear/damage"

	case "1001":
		remarkID = "Pengirim akan mengantar paket ke cabang"
		remarkEN = "Shipper will drop the parcels to the branch"
	case "1002":
		remarkID = "Pengirim tidak ada di lokasi/toko"
		remarkEN = "Sender is not at home/store"
	case "1003":
		remarkID = "Alamat pengirim kurang jelas"
		remarkEN = "Sender address is not valid"
	case "1004":
		remarkID = "Pengirim sedang mempersiapkan paket"
		remarkEN = "Sender is preparing the parcels"
	case "1005":
		remarkID = "Pengirim meminta pergantian jadwal"
		remarkEN = "Sender request to reschedule"
	case "1007":
		remarkID = "Di luar cakupan area cabang, akan dijadwalkan ke cabang lain"
		remarkEN = "Out of branch coverage, will be scheduled to other branch"
	case "1008":
		remarkID = "Melewati jam operasional cabang"
		remarkEN = "Exceeded branch business hours"
	case "1010":
		remarkID = "Cuaca buruk / bencana alam"
		remarkEN = "Force Majeur"
	case "1011":
		remarkID = "Pengirim merasa tidak menggunakan iDexpress"
		remarkEN = "Sender is not iDexpress customer"
	case "1012":
		remarkID = "Pengirim sedang libur"
		remarkEN = "Sender is on holiday"
	case "1014":
		remarkID = "Paket pre order"
		remarkEN = "Pre order parcels"
	case "1015":
		remarkID = "Paket yang ingin diambil sudah dalam keadaan rusak"
		remarkEN = "Package is damaged before being picked up"
	case "1016":
		remarkID = "Pengiriman dibatalkan sebelum di pickup"
		remarkEN = "Shipment was cancelled before pickup"

	case "6001":
		remarkID = "Kerusakan pada resi / informasi resi tidak jelas"
		remarkEN = "Information on AWB is unclear/damage"
	case "6002":
		remarkID = "Berat paket tidak sesuai"
		remarkEN = "Weight Discrepancy"
	case "4002":
		remarkID = "Pengiriman dibatalkan"
		remarkEN = "Shipment cancellation"
	case "4003":
		remarkID = "Paket akan diproses dengan nomor resi yang baru"
		remarkEN = "Parcels will be ship with new Airwaybill"
	case "5002":
		remarkID = "Penundaan jadwal armada pengiriman"
		remarkEN = "Vehicle departure schedule delayed"
	case "5017":
		remarkID = "Kerusakan pada resi / informasi resi tidak jelas"
		remarkEN = "Information on AWB is unclear/damage"
	case "5020":
		remarkID = "Berat paket tidak sesuai"
		remarkEN = "Weight Discrepancy"
	case "5015":
		remarkID = "Kemasan paket tidak sesuai prosedur"
		remarkEN = "Packaging does not meet the standard"
	case "5011":
		remarkID = "Kemasan paket rusak"
		remarkEN = "Packaging is damage"
	case "5005":
		remarkID = "Paket akan dikembalikan ke cabang asal"
		remarkEN = "Parcels will be return to origin branch"
	case "5014":
		remarkID = "Pengiriman dibatalkan"
		remarkEN = "Shipment cancellation"
	case "5013":
		remarkID = "Paket ditolak oleh bea cukai (red line)"
		remarkEN = "Rejected by customs"

	case "5018":
		remarkID = "Terdapat barang berbahaya (Dangerous Goods)"
		remarkEN = "Dangerous Goods"
	case "5016":
		remarkID = "Paket salah dalam proses penyortiran"
		remarkEN = "Miss Sort"
	case "5019":
		remarkID = "Paket rusak/pecah"
		remarkEN = "Damaged parcels"
	case "5006":
		remarkID = "Paket hilang atau tidak ditemukan"
		remarkEN = "Parcels is lost or cannot be found"
	case "5007":
		remarkID = "Data alamat tidak sesuai dengan kode sortir"
		remarkEN = "Destination address does not match with sort code"
	case "5012":
		remarkID = "Paket crosslabel"
		remarkEN = "Crosslabel parcels"
	case "5021":
		remarkID = "Food parcels, kept until proper delivery time"
		remarkEN = "Food parcels, kept until proper delivery time"
	case "6003":
		remarkID = "Paket makanan, disimpan hingga waktu pengiriman yang tepat"
		remarkEN = "Food parcels, kept until proper delivery time"
	case "6004":
		remarkID = "Cuaca buruk / bencana alam"
		remarkEN = "Bad weather or force majeure"

	default:
		remarkID = "Problem On Shipment scan"
		remarkEN = "Problem On Shipment scan"
	}
	return
}

// func MappingStatusCodeProblemIDExpress(problemCode string) (code int, remark string) {

// 	switch problemCode {

// 	case "3001", "3002", "3003", "3004", "3005", "3007", "3008", "3011", "3014", "3015", "3018", "6006", "6007", "6008", "6010", "1002", "1003", "5014", "5013", "5018", "5016", "5007", "5012", "5021", "6003", "5002":
// 		code = ERR_UNDELIVERY
// 		remark = "UNDELIVERY"
// 	case "3012", "3017":
// 		code = ERR_BROKEN
// 		remark = "BROKEN"
// 	case "6009", "1010", "6004":
// 		code = ERR_FORCE_MAJEUR
// 		remark = "FORCE MAJEUR"
// 	case "1001", "1004":
// 		code = ERR_CREATE_PICK
// 		remark = "PICK"
// 	case "1005", "1007", "1008", "1011", "1012", "1014", "1015", "1016", "4002", "4003":
// 		code = ERR_UNPICK
// 		remark = "UNPICK"
// 	case "6001", "5017":
// 		code = ERR_INVALID_ORDER
// 		remark = "INVALID ORDER"
// 	case "6002", "5020", "5015", "5011":
// 		code = ERR_PACKAGE_PROBLEM
// 		remark = "PACKAGE PROBLEM"
// 	case "5005":
// 		code = ERR_RETURN
// 		remark = "RETURN"
// 	case "5019":
// 		code = ERR_BROKEN
// 		remark = "BROKEN"
// 	case "5006":
// 		code = ERR_LOST
// 		remark = "LOST"
// 	default:
// 		code = ERR_UNDELIVERY
// 		remark = "UNDELIVERY"
// 	}
// 	return
// }

func MappingStatusCodeProblemIDExpress(problemCode string) (code int, remark string) {

	switch problemCode {

	case "50012", "50007", "50006", "50005", "50004", "40001", "30009", "30007", "30006", "30004", "30003", "30002", "30001", "6010", "6008", "6007", "6006", "6005",
		"5018", "5016", "5014", "5013", "5012", "5007", "5005", "4003", "4002", "3018", "3017", "3015", "3014", "3011", "3008", "3007", "3005", "3004", "3003", "3002", "3001":
		code = ERR_UNDELIVERY
		remark = "UNDELIVERY"
	case "50011", "30008", "6009", "6004":
		code = ERR_FORCE_MAJEUR
		remark = "FORCE MAJEUR"
	case "50010", "50001", "6003", "5017", "5015", "5002", "5021":
		code = ERR_HOLD
		remark = "HOLD"
	case "3012", "5011", "5019", "30005", "50003", "50008":
		code = ERR_BROKEN
		remark = "BROKEN"
	case "5020", "50009":
		code = ERR_PACKAGE_PROBLEM
		remark = "PACKAGE PROBLEM"
	case "5006", "50002":
		code = ERR_LOST
		remark = "LOST"
	case "1004", "10004":
		code = ERR_CREATE_PICK
		remark = "PICK"
	case "10008", "10007", "10006", "10005", "10003", "10002", "10001", "6002", "6001", "1016", "1015", "1014", "1012", "1011", "1010", "1008", "1007", "1005", "1003", "1002", "1001":
		code = ERR_UNPICK
		remark = "UNPICK"
	default:
		code = ERR_UNDELIVERY
		remark = "UNDELIVERY"
	}
	return
}
