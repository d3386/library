package library

const (
	ERR_SUCCESS                   int = 0 // succes/delivered/success payment
	ERR_PARAM_MISSING             int = 1
	ERR_PARAM_ILLEGAL             int = 2
	ERR_INVALID_FORMAT            int = 3
	ERR_DATABASE                  int = 4
	ERR_UNAUTHORIZED              int = 5
	ERR_USER_NOT_FOUND            int = 6
	ERR_SELLER_NOT_FOUND          int = 7
	ERR_USER_EXIST                int = 8
	ERR_MENU_NOT_FOUND            int = 9
	ERR_UID_NOT_FOUND             int = 10
	ERR_MAX_LENGTH                int = 11
	ERR_ORDER_EXIST               int = 13
	ERR_PICKUP_CONTACT_NOT_FOUND  int = 14
	ERR_TRANSACTION_NOT_FOUND     int = 15
	ERR_ORDER_LIST_NOT_FOUND      int = 16
	ERR_ORDER_ID_NOT_FOUND        int = 21
	ERR_SHIPPING_COST_NOT_FOUND   int = 22
	ERR_INPROGRESS                int = 99
	ERR_OTP_NOT_FOUND             int = 30
	ERR_CITY_NOT_FOUND            int = 31
	ERR_MAX_ROW_SPREADSHEET       int = 32
	ERR_PHONE_NOT_FOUND           int = 33
	ERR_TICKET_STILL_ACTIVE       int = 34
	ERR_TICKET_HAS_CLOSED         int = 35
	ERR_TICKET_NOT_FOUND          int = 36
	ERR_OTHERS                    int = 100
	ERR_MINIMUM_WEIGHT            int = 40
	ERR_MAX_WEIGHT                int = 46
	ERR_COURIER_NOT_FOUND         int = 41
	ERR_SERVICE_NOT_FOUND         int = 42
	ERR_RECEIPT_NOT_FOUND         int = 43
	ERR_VEHICLE_NOT_FOUND         int = 44
	ERR_ESTIMATED_PRICE_NOT_FOUND int = 20
	ERR_INVALID_STATUS            int = 23
	ERR_TARIF_NOT_FOUND           int = 45
	ERR_INVALID_EMAIL             int = 47
	ERR_SYSTEM_ERROR              int = 150
	ERR_MIN_LENGTH                int = 24

	//status tracking untuk kolom status di database
	ERR_CREATE_ORDER          int = 12  // Success create order tetapi belum hit API ekspedisi
	ERR_SUCCESS_CREATE_ORDER  int = 17  // Success create order dan sukses hit API ekspedisi
	ERR_CREATE_PICK           int = 101 // Sudah sukses hit API pickup & sudah dapat resi, tetapi belum di pickup & scan oleh ekspedisi (jne)
	ERR_CREATE_PICKED         int = 102 // Sudah di scan/pickup
	ERR_ON_PROCESS            int = 103 // Paket sedang proses, dan tidak bermasalah
	ERR_UNDELIVERY            int = 104 // Paket bermasalah, butuh perhatian
	ERR_RETURN                int = 105 // Proses return
	ERR_RETURNED              int = 106 // Paket sudah di terima oleh seller/ sukses dibalikin (last status final)
	ERR_LOST                  int = 107 // Paket yg dinyatakan hilang, 90% kejadian (last status final)
	ERR_BROKEN                int = 108 // Paket pecah/rusak, rusak di gerai/penerima
	ERR_BREACH                int = 109 // Claim sudah melewati batas SLA, jarang breach, jarang terjadi
	ERR_CANCEL_PICKUP         int = 110 // Cancel khusus pickup (last status final)
	ERR_FORCE_MAJEUR          int = 111 // Bencana Alam
	ERR_DESTROYED             int = 112 // barang tidak diambil atau alamat nya ketika di return itu sudah pindah dll itu pun yg sudah lama banget (last status final)
	ERR_HOLD                  int = 113 // cth barang ke tahan di bandara nunggu jadwal penerbangan biasa nya mereka pake status hold/ penerima ga ada dirumah
	ERR_UNPICK                int = 114
	ERR_DROP                  int = 115
	ERR_DELIVERED             int = 116 // Yang Bersangkutan
	ERR_FAILED_ORDER          int = 18  // Success create order tetapi failed hit API ekspedisi
	ERR_TIMEOUT_ORDER         int = 19  // Success create order tetapi timeout hit API ekspedisi
	ERR_CANCEL                int = 117 // Cancel order/pickup. JNE: Cancel order, SICEPAT: Cancel Pickup
	ERR_REQUEST_CANCEL        int = 118 // Request cancel to utility cancel pickup (ERR_SUCCESS_CREATE_ORDER, ERR_CREATE_PICK)
	ERR_REQUEST_CANCEL_CREATE int = 119 // Request cancel to utility cancel pickup (ERR_SUCCESS_CREATE_ORDER, ERR_CREATE_PICK)
	ERR_INVALID_ORDER         int = 120
	ERR_PACKAGE_PROBLEM       int = 121

	// error bulk order untul kolom properties di database
	ERR_BULK_ORDER_MISSING_RECEIVE_NAME     int = 51
	ERR_BULK_ORDER_INVALID_RECEIVE_NAME     int = 52
	ERR_BULK_ORDER_MISSING_RECEIVE_PHONE    int = 53
	ERR_BULK_ORDER_INVALID_RECEIVE_PHONE    int = 54
	ERR_BULK_ORDER_MISSING_RECEIVE_ADDRESS  int = 55
	ERR_BULK_ORDER_INVALID_RECEIVE_ADDRESS  int = 56
	ERR_BULK_ORDER_MISSING_PRODUCT_NAME     int = 57
	ERR_BULK_ORDER_MISSING_PRODUCT_WEIGHT   int = 58
	ERR_BULK_ORDER_MISSING_PRODUCT_PRICE    int = 59
	ERR_BULK_ORDER_MISSING_PRODUCT_QUANTITY int = 60
	ERR_BULK_ORDER_MISSING_SUBDISTRICT_ID   int = 61
	ERR_BULK_ORDER_SUBDISTRICT_ID_NOT_FOUND int = 62
	// error bulk order untuk kolom status di database
	ERR_FAILED_BULK_ORDER int = 70

	// payment
	ERR_NOT_ENOUGH_BALANCE               int = 200
	ERR_PAYMENT_DECLINED                 int = 201
	ERR_PAYMENT_DUPLICATE                int = 203
	ERR_PAYMENT_WAITING                  int = 204 // setelah insert ke database
	ERR_PAYMENT_DETECTED                 int = 205
	ERR_PAYMENT_EXPIRED                  int = 206
	ERR_PAYMENT_METHOD_NOT_FOUND         int = 207
	ERR_PAYMENT_PENDING                  int = 208 // sebelum insert ke database
	ERR_PAYMENT_METHOD_ACCOUNT_NOT_FOUND int = 209
	ERR_PAYMENT_METHOD_ACCOUNT_EXIST     int = 210
	ERR_TOPUP_NOT_FOUND                  int = 211
	ERR_VA_STILL_ACTIVE                  int = 212
	ERR_ACCOUNT_NOT_FOUND                int = 213

	// va-aggregator
	ERR_PAYMENT_VA_EMPTY  int = 216
	ERR_PAYMENT_VA_ACTIVE int = 217

	// LOAN SYSTEM
	ERR_LOAN_APPROVAL         int = 0
	ERR_LOAN_PENDING          int = 1
	ERR_LOAN_PROCESSING       int = 3
	ERR_LOAN_REJECT           int = 4
	ERR_LOAN_NOT_ALLOW_SYSTEM int = 100

	// LOAN DETAIL SYSTEM
	ERR_LOAN_DETAIL_VALID     int = 0
	ERR_LOAN_DETAIL_NOT_VALID int = 100
)
