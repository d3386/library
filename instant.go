package library

type InstantEstimatedTarif struct {
	ServiceCode       string            `json:"service_code" validate:"required"`
	Pickup            InstantPickup     `json:"pickup" validate:"required"`
	Receiver          []InstantReceiver `json:"receiver" validate:"required"`
	AdditionalService []string          `json:"additional_service"`
}

type InstantPickup struct {
	Timestamp string `json:"timestamp" validate:"required"`
	Latitude  string `json:"lat" validate:"required"`
	Longitude string `json:"long" validate:"required"`
	Address   string `json:"address" validate:"required"`
}

type InstantReceiver struct {
	Latitude  string `json:"lat" validate:"required"`
	Longitude string `json:"long" validate:"required"`
	Address   string `json:"address" validate:"required"`
}

type InstantCreateOrder struct {
	ServiceCode       string                 `json:"service_code" validate:"required"`
	Pickup            InstantPickupOrder     `json:"pickup" validate:"required"`
	Receiver          []InstantReceiverOrder `json:"receiver" validate:"required"`
	AdditionalService []string               `json:"additional_service"`
}

type InstantPickupOrder struct {
	Timestamp string `json:"timestamp" validate:"required"`
	Name      string `json:"name" validate:"required"`
	Phone     string `json:"phone" validate:"required"`
	Latitude  string `json:"lat" validate:"required"`
	Longitude string `json:"long" validate:"required"`
	Address   string `json:"address" validate:"required"`
}

type InstantReceiverOrder struct {
	Name        string `json:"name" validate:"required"`
	Phone       string `json:"phone" validate:"required"`
	Latitude    string `json:"lat" validate:"required"`
	Longitude   string `json:"long" validate:"required"`
	Address     string `json:"address" validate:"required"`
	Description string `json:"description" validate:"required"`
}
