package library

type RequestMessage struct {
	Order Order `json:"order,omitempty"`
}
type Order struct {
	ID              int64    `json:"id,omitempty"`
	Courier         string   `json:"courier,omitempty"`
	ReceiptNo       string   `json:"receipt_no,omitempty"`
	OrderNo         string   `json:"order_no,omitempty"`
	ServiceCode     string   `json:"service_code,omitempty"`
	PickupMethod    string   `json:"pickup_method,omitempty"`
	Vehicle         string   `json:"vehicle,omitempty"`
	Account         Account  `json:"-"`
	Pickup          Pickup   `json:"pickup,omitempty"`
	Shipper         Shipper  `json:"shipper,omitempty"`
	Receiver        Receiver `json:"receiver,omitempty"`
	Seller          Seller   `json:"seller,omitempty"`
	OriginCode      string   `json:"origin_code,omitempty"`
	DestinationCode string   `json:"destination_code,omitempty"`
	Product         Product  `json:"product,omitempty"`
	IsInsurance     bool     `json:"is_insurance,omitempty"`
	IsCod           bool     `json:"is_cod,omitempty"`
	CodAmount       int64    `json:"cod_amount,omitempty"`
	CustomerCode    string   `json:"customer_code,omitempty"`
	Description     string   `json:"description,omitempty"`
	Timestamp       string   `json:"timestamp,omitempty"`
}

type Account struct {
	ID string `json:"id,omitempty"`
}

type Pickup struct {
	ID          int64       `json:"id,omitempty"`
	Name        string      `json:"name,omitempty"`
	Timestamp   string      `json:"timestamp,omitempty"`
	Phone       string      `json:"phone,omitempty"`
	Email       string      `json:"email,omitempty"`
	Address     string      `json:"address,omitempty"`
	Country     Country     `json:"country,omitempty"`
	Province    Province    `json:"province,omitempty"`
	City        City        `json:"city,omitempty"`
	OriginCode  string      `json:"origin_code,omitempty"`
	District    District    `json:"district,omitempty"`
	Subdistrict Subdistrict `json:"sub_district,omitempty"`
	Zip         string      `json:"zip,omitempty"`
	Latitude    string      `json:"latitude,omitempty"`
	Longitude   string      `json:"longitude,omitempty"`
}

type Shipper struct {
	Name        string      `json:"name,omitempty"`
	Phone       string      `json:"phone,omitempty"`
	Email       string      `json:"email,omitempty"`
	Address     string      `json:"address,omitempty"`
	Country     Country     `json:"country,omitempty"`
	Province    Province    `json:"province,omitempty"`
	City        City        `json:"city,omitempty"`
	District    District    `json:"district,omitempty"`
	Subdistrict Subdistrict `json:"sub_district,omitempty"`
	Zip         string      `json:"zip,omitempty"`
	Latitude    string      `json:"latitude,omitempty"`
	Longitude   string      `json:"longitude,omitempty"`
	Description string      `json:"description,omitempty"`
}

type Receiver struct {
	Name            string      `json:"name,omitempty"`
	Phone           string      `json:"phone,omitempty"`
	Address         string      `json:"address,omitempty"`
	Country         Country     `json:"country,omitempty"`
	Province        Province    `json:"province,omitempty"`
	City            City        `json:"city,omitempty"`
	DestinationCode string      `json:"destination_code,omitempty"`
	District        District    `json:"district,omitempty"`
	Subdistrict     Subdistrict `json:"sub_district,omitempty"`
	Zip             string      `json:"zip,omitempty"`
	Latitude        string      `json:"latitude,omitempty"`
	Longitude       string      `json:"longitude,omitempty"`
	Description     string      `json:"description,omitempty"`
}

type Product struct {
	Sku         string  `json:"sku,omitempty"`
	Name        string  `json:"name,omitempty"`
	Category    string  `json:"category,omitempty"`
	Weight      float64 `json:"weight,omitempty"`
	Length      float64 `json:"length,omitempty"`
	Width       float64 `json:"width,omitempty"`
	Height      float64 `json:"height,omitempty"`
	Volume      string  `json:"volume,omitempty"`
	Quantity    int64   `json:"quantity,omitempty"`
	Price       int64   `json:"price,omitempty"`
	Description string  `json:"description,omitempty"`
}

type EstimatedTarifList struct {
	Status int              `json:"status"`
	Price  []EstimatedTarif `json:"data,omitempty"`
}

type EstimatedTarif struct {
	Courier       string        `json:"courier,omitempty"`
	ServiceCode   string        `json:"service_code,omitempty"`
	Weight        float64       `json:"weight,omitempty"`
	Origin        Origin        `json:"origin,omitempty"`
	Destination   Destination   `json:"destination,omitempty"`
	EstimatedTime EstimatedTime `json:"estimated_time,omitempty"`
	Product       Product       `json:"product,omitempty"`
	Price         int64         `json:"price,omitempty"`
}

type Tracking struct {
	ReceiptNo      string           `json:"receipt_no,omitempty"`
	ReferenceNo    string           `json:"reference_no,omitempty"`
	Origin         Origin           `json:"origin,omitempty"`
	Destination    Destination      `json:"destination,omitempty"`
	Pod            Pod              `json:"pod,omitempty"`
	Service        Service          `json:"service,omitempty"`
	Weight         float64          `json:"weight,omitempty"`
	EstimatedTime  EstimatedTime    `json:"estimated_time,omitempty"`
	Pickup         TrackingPickup   `json:"pickup,omitempty"`
	Receiver       TrackingReceiver `json:"receiver,omitempty"`
	Description    string           `json:"description,omitempty"`
	Signature      string           `json:"signature,omitempty"`
	Photo          string           `json:"photo,omitempty"`
	Reason         string           `json:"reason"`
	DeliveredDate  string           `json:"delivered_date"`
	DetailTracking []DetailTracking `json:"detail_tracking,omitempty"`
}

type TrackingPickup struct {
	Name    string `json:"name,omitempty"`
	Address string `json:"address,omitempty"`
}

type TrackingReceiver struct {
	Name           string `json:"name,omitempty"`
	Address        string `json:"address,omitempty"`
	ActualReceiver string `json:"actual_receiver"`
}

type DetailTracking struct {
	Status       StatusTracking       `json:"status_tracking"`
	OriginStatus OriginStatusTracking `json:"origin_status_tracking"`
}

type StatusTracking struct {
	Code   int    `json:"code"`
	Remark string `json:"remark"`
}

type OriginStatusTracking struct {
	Code      string `json:"code"`
	Remark    string `json:"remark"`
	Timestamp string `json:"timestamp,omitempty"`
}

type Service struct {
	Code string `json:"code,omitempty"`
	Type string `json:"type,omitempty"`
}
type Pod struct {
	Code        int    `json:"code"`
	Status      string `json:"status"`
	Description string `json:"description,omitempty"`
}

type EstimatedTime struct {
	From        int    `json:"from,omitempty"`
	To          int    `json:"thru,omitempty"`
	Description string `json:"description,omitempty"`
}

type Receipt struct {
	ReceiptNo string `json:"receipt_no,omitempty"`
}
type Seller struct {
	ID    string `json:"id,omitempty"`
	Name  string `json:"name,omitempty"`
	Email string `json:"email,omitempty"`
}
type Country struct {
	ID     int64  `json:"id,omitempty"`
	Name   string `json:"name,omitempty"`
	Config string `json:"config,omitempty"`
}

type Province struct {
	ID     int64  `json:"id,omitempty"`
	Name   string `json:"name,omitempty"`
	Config string `json:"config,omitempty"`
}

type City struct {
	ID     int64  `json:"id,omitempty"`
	Code   string `json:"code,omitempty"`
	Name   string `json:"name,omitempty"`
	Config string `json:"config,omitempty"`
}

type District struct {
	ID     int64          `json:"id,omitempty"`
	Code   string         `json:"code,omitempty"`
	Name   string         `json:"name,omitempty"`
	Config DistrictConfig `json:"config,omitempty"`
}

type Subdistrict struct {
	ID     int64  `json:"id,omitempty"`
	Name   string `json:"name,omitempty"`
	Config string `json:"config,omitempty"`
}

type DistrictConfig struct {
	Branch Branch `json:"branch,omitempty"`
}

type Branch struct {
	Code string `json:"code,omitempty"`
	Name string `json:"name,omitempty"`
}

type Origin struct {
	Code string `json:"code,omitempty"`
	Name string `json:"name,omitempty"`
}
type Destination struct {
	Code string `json:"code,omitempty"`
	Name string `json:"name,omitempty"`
}

// oy
type BalanceData struct {
	Name    string  `json:"name"`
	Balance Balance `json:"balance"`
}

type Balance struct {
	Remaining Remaining `json:"remaining"`
	Pending   Pending   `json:"pending"`
	Available Available `json:"available"`
}

type Remaining struct {
	Currency string  `json:"currency"`
	Value    float64 `json:"value"`
}

type Pending struct {
	Currency string  `json:"currency"`
	Value    float64 `json:"value"`
}

type Available struct {
	Currency string  `json:"currency"`
	Value    float64 `json:"value"`
}

type Payment struct {
	ReferenceID string      `json:"reference_id"`
	Account     User        `json:"account"`
	Payment     PaymentData `json:"payment"`
	Description string      `json:"description"`
}

type User struct {
	ID        int64  `json:"id,omitempty"`
	Name      string `json:"name,omitempty"`
	Email     string `json:"email"`
	AccountNo string `json:"account_no"`
	BankCode  string `json:"bank_code"`
}

type PaymentData struct {
	Method   string `json:"method"`
	BankCode string `json:"bank_code"`
	Amount   int    `json:"amount"`
	Account  string `json:"account"`
}

type ResponseVirtualAccountAgregator struct {
	ID                     string  `json:"id,omitempty"`
	Amount                 float64 `json:"amount,omitempty"`
	Status                 Status  `json:"status"`
	VANumber               string  `json:"va_number,omitempty"`
	BankCode               string  `json:"bank_code,omitempty"`
	IsOpen                 bool    `json:"is_open,omitempty"`
	IsSingleUse            bool    `json:"is_single_use,omitempty"`
	ExpirationTime         int     `json:"expiration_time,omitempty"`
	VAStatus               string  `json:"va_status,omitempty"`
	UsernameDisplay        string  `json:"username_display,omitempty"`
	PartnerUserID          string  `json:"partner_user_id,omitempty"`
	CounterIncomingPayment int     `json:"counter_incoming_payment,omitempty"`
	TrxExpirationTime      int     `json:"trx_expiration_time,omitempty"`
	TrxCounter             int     `json:"trx_counter,omitempty"`
}

type Status struct {
	Code    string `json:"code"`
	Message string `json:"message"`
}

type VirtualAccount struct {
	AccountName string `json:"name,omitempty"`
	BankCode    string `json:"bank_code,omitempty"`
	AccountNo   string `json:"account_no,omitempty"`
	VANumber    string `json:"va_number,omitempty"`
	ReferenceID string `json:"reference_id,omitempty"`
	Status      string `json:"status,omitempty"`
}
